#define _POSIX_C_SOURCE 200809L
#include <errno.h>
#include <fcntl.h>
#include <libiberty/libiberty.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(void) {
	char *s = concat("foo", "bar", "bat", "baz", "cat", "ding", "dong", NULL);
	if(puts(s) == EOF) {
		perror("Failed to print string");
		abort();
	}
	free(s);

	if(faccessat(AT_FDCWD, "/proc/self/fd/0", R_OK, AT_EACCESS) != -1) {
		const int zerofd = open("/proc/self/fd/0", O_RDONLY);
		if(zerofd == -1) {
			perror("Failed to open /proc/self/fd/0");
			abort();
		}
		if(!fdmatch(0, zerofd)) {
			fputs("fds do not match!\n", stderr);
			abort();
		}
		if(close(zerofd) == -1) {
			perror("Failed to close file descriptor");
			abort();
		}
	}

	if(puts(getpwd()) == EOF) {
		perror("Failed to print working directory");
		abort();
	}
	if(EINVAL != strtoerrno("EINVAL")) {
		fputs("strtoerrno failed\n", stderr);
		abort();
	}
	if(strcmp("ERANGE", strerrno(ERANGE))) {
		fputs("strerrno failed\n", stderr);
		abort();
	}
	if(strcmp("SIGSEGV", strsigno(SIGSEGV))) {
		fputs("strsigno failed\n", stderr);
		abort();
	}
	if(SIGABRT != strtosigno("SIGABRT")) {
		fputs("strtosigno failed\n", stderr);
		abort();
	}

	s = xasprintf("%d", 0);
	if(puts(s) == EOF) {
		perror("Failed to print string");
		abort();
	}
	if(strcmp("0", s)) {
		fputs("xasprintf gave unexpected results\n", stderr);
		abort();
	}
	free(s);


}
