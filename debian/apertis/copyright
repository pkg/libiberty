Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 1995-2008, 2010, 2011, 2014, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-2+ with Autoconf-data exception

Files: config/ax_check_define.m4
Copyright: 2008, Guido U. Draheim <guidod@gmx.de>
License: GPL-3+ with Autoconf-2.0~Archive exception

Files: config/ax_count_cpus.m4
Copyright: 2014, 2016, Karlson2k (Evgeny Grin) <k2k@narod.ru>
 2012, Brian Aker <brian@tangent.org>
 2008, Michael Paul Bailey <jinxidoru@byu.net>
 2008, Christophe Tournayre <turn3r@users.sourceforge.net>
License: FSFAP

Files: config/ax_cxx_compile_stdcxx.m4
Copyright: 2020, Jason Merrill <jason@redhat.com>
 2019, Enji Cooper <yaneurabeya@gmail.com>
 2016, 2018, Krzesimir Nowak <qdlacz@gmail.com>
 2015, Paul Norman <penorman@mac.com>
 2015, Moritz Klammler <moritz@klammler.eu>
 2014, 2015, Google Inc.; contributedAlexey Sokolov <sokolov@google.com>
 2013, Roy Stogner <roystgnr@ices.utexas.edu>
 2012, Zack Weinberg <zackw@panix.com>
 2008, Benjamin Kosnik <bkoz@redhat.com>
License: FSFAP

Files: config/ax_pthread.m4
Copyright: 2011, Daniel Richard G. <skunk@iSKUNK.ORG>
 2008, Steven G. Johnson <stevenj@alum.mit.edu>
License: GPL-3+ with Autoconf-2.0~Archive exception

Files: config/bfd64.m4
 config/dfp.m4
 config/lead-dot.m4
 config/toolexeclibdir.m4
 config/warnings.m4
Copyright: 1986-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3+

Files: config/bitfields.m4
 config/codeset.m4
 config/debuginfod.m4
 config/elf.m4
 config/gc++filt.m4
 config/gcc-plugin.m4
 config/glibc21.m4
 config/gthr.m4
 config/intdiv0.m4
 config/inttypes-pri.m4
 config/inttypes.m4
 config/inttypes_h.m4
 config/ld-symbolic.m4
 config/lib-ld.m4
 config/lib-link.m4
 config/lib-prefix.m4
 config/lthostflags.m4
 config/stdint_h.m4
 config/ulonglong.m4
 config/zstd.m4
Copyright: 1996-2020, 2022, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL

Files: config/depstand.m4
 config/extensions.m4
 config/multi.m4
Copyright: 1996-2017, Free Software Foundation, Inc. <http://fsf.org/>
License: FSFULLR

Files: config/gettext.m4
 config/intlmacosx.m4
Copyright: 1995-2014, 2016, 2018-2023, Free Software Foundation, Inc. <http://fsf.org/>
License: FSFULLR or GPL or LGPL

Files: config/iconv.m4
Copyright: 2000-2002, 2007-2014, 2016-2023, Free Software Foundation
License: FSFULLR

Files: config/isl.m4
 config/libstdc++-raw-cxx.m4
Copyright: no-info-found
License: GPL-3+

Files: config/lcmessage.m4
 config/nls.m4
 config/po.m4
 config/progtest.m4
Copyright: 1995-2003, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL or LGPL

Files: config/pkg.m4
Copyright: 2012-2015, Dan Nicholson <dbn.lists@gmail.com>
 2004, Scott James Remnant <scott@netsplit.com>.
License: GPL-2+ with Autoconf-data exception

Files: debian/*
Copyright: 2022, John Scott <jscott@posteo.net>
 2013-2023, Matthias Klose <doko@debian.org>
License: GPL-2+

Files: include/*
Copyright: 1989, 1991, 2007, 2009-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-2 or GPL-3

Files: include/COPYING
Copyright: 1989, 1991, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-2

Files: include/COPYING3
Copyright: 2007, 2009-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3

Files: include/ChangeLog
 include/ChangeLog-0415
 include/ChangeLog-2016
 include/ChangeLog-2017
 include/ChangeLog-2018
 include/ChangeLog-2019
 include/ChangeLog-2020
 include/ChangeLog-9103
 include/MAINTAINERS
Copyright: 1991-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: FSFAP

Files: include/alloca-conf.h
 include/bfdlink.h
 include/binary-io.h
 include/bout.h
 include/collectorAPI.h
 include/ctf-api.h
 include/ctf.h
 include/diagnostics.h
 include/dis-asm.h
 include/dwarf2.def
 include/dwarf2.h
 include/gcc-c-fe.def
 include/gcc-c-interface.h
 include/gcc-cp-fe.def
 include/gcc-cp-interface.h
 include/gcc-interface.h
 include/hp-symtab.h
 include/libcollector.h
 include/libfcollector.h
 include/lto-symtab.h
 include/oasys.h
 include/plugin-api.h
 include/sframe-api.h
 include/sframe.h
 include/sha1.h
 include/xtensa-isa-internal.h
 include/xtensa-isa.h
Copyright: 1986-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3+

Files: include/ansidecl.h
 include/dyn-string.h
 include/fibheap.h
 include/filenames.h
 include/floatformat.h
 include/fnmatch.h
 include/fopen-bin.h
 include/fopen-same.h
 include/fopen-vms.h
 include/getopt.h
 include/hashtab.h
 include/md5.h
 include/objalloc.h
 include/os9k.h
 include/partition.h
 include/simple-object.h
 include/sort.h
 include/splay-tree.h
 include/symcat.h
 include/xtensa-config.h
 include/xtensa-dynconfig.h
Copyright: 1987-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-2+

Files: include/aout/*
Copyright: 1986-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3+

Files: include/aout/ChangeLog-9115
Copyright: 1991-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: FSFAP

Files: include/cgen/*
Copyright: 1986-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3+

Files: include/cgen/ChangeLog-0915
Copyright: 1991-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: FSFAP

Files: include/coff/*
Copyright: 1986-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3+

Files: include/coff/ChangeLog-0415
 include/coff/ChangeLog-9103
Copyright: 1991-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: FSFAP

Files: include/demangle.h
 include/vtv-change-permission.h
Copyright: 1989-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: LGPL-2+ with GCC-exception-2.0 exception

Files: include/elf/*
Copyright: 1986-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3+

Files: include/elf/ChangeLog-0415
 include/elf/ChangeLog-9103
Copyright: 1991-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: FSFAP

Files: include/elf/aarch64.h
 include/elf/kvx.h
 include/elf/kvx_elfids.h
 include/elf/loongarch.h
Copyright: 2007, 2009-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3

Files: include/elf/xgate.h
Copyright: 1987-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-2+

Files: include/environ.h
 include/leb128.h
 include/safe-ctype.h
 include/timeval-utils.h
Copyright: 1989-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: LGPL-2+

Files: include/gdb/*
Copyright: 1986-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3+

Files: include/gdb/ChangeLog
Copyright: 1991-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: FSFAP

Files: include/libiberty.h
Copyright: does not belong to the
 1997-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-2+

Files: include/longlong.h
Copyright: 1991-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: LGPL-2.1+ with GCC-exception-2.0 exception

Files: include/mach-o/*
Copyright: 1986-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3+

Files: include/mach-o/ChangeLog-1115
Copyright: 1991-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: FSFAP

Files: include/obstack.h
 include/xregex2.h
Copyright: 1985-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: LGPL-2.1+

Files: include/opcode/*
Copyright: 1986-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3+

Files: include/opcode/ChangeLog-0415
Copyright: 1991-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: FSFAP

Files: include/opcode/aarch64.h
 include/opcode/kvx.h
 include/opcode/loongarch.h
Copyright: 2007, 2009-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3

Files: include/opcode/ft32.h
Copyright: 1987-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-2+

Files: include/sim/*
Copyright: 1986-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3+

Files: include/som/*
Copyright: 1986-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3+

Files: include/som/ChangeLog-1015
Copyright: 1991-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: FSFAP

Files: include/vms/*
Copyright: 1986-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3+

Files: include/vms/ChangeLog-1015
Copyright: 1991-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: FSFAP

Files: install-sh
Copyright: 1994, X Consortium
License: X11

Files: libiberty/*
Copyright: 1991-2024, Free Software Foundation, Inc.
 1991, 1999, Free Software Foundation, Inc. <http://fsf.org/>
License: LGPL-2.1

Files: libiberty/ChangeLog.jit
Copyright: footer.
License: FSFAP

Files: libiberty/Makefile.in
 libiberty/argv.c
 libiberty/asprintf.c
 libiberty/choose-temp.c
 libiberty/concat.c
 libiberty/fdmatch.c
 libiberty/fopen_unlocked.c
 libiberty/gather-docs
 libiberty/getruntime.c
 libiberty/hashtab.c
 libiberty/hex.c
 libiberty/lbasename.c
 libiberty/maint-tool
 libiberty/make-temp-file.c
 libiberty/mempcpy.c
 libiberty/mkstemps.c
 libiberty/pex-common.c
 libiberty/pex-common.h
 libiberty/pex-djgpp.c
 libiberty/pex-msdos.c
 libiberty/pex-one.c
 libiberty/pex-unix.c
 libiberty/pex-win32.c
 libiberty/pexecute.c
 libiberty/putenv.c
 libiberty/safe-ctype.c
 libiberty/setenv.c
 libiberty/setproctitle.c
 libiberty/simple-object-common.h
 libiberty/spaces.c
 libiberty/stpcpy.c
 libiberty/stpncpy.c
 libiberty/strndup.c
 libiberty/timeval-utils.c
 libiberty/vasprintf.c
 libiberty/vprintf-support.c
 libiberty/vprintf-support.h
 libiberty/xasprintf.c
 libiberty/xexit.c
 libiberty/xmalloc.c
 libiberty/xstrndup.c
 libiberty/xvasprintf.c
Copyright: 1989-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: LGPL-2+

Files: libiberty/_doprnt.c
 libiberty/clock.c
 libiberty/fibheap.c
 libiberty/filedescriptor.c
 libiberty/filename_cmp.c
 libiberty/floatformat.c
 libiberty/fnmatch.c
 libiberty/getopt.c
 libiberty/getopt1.c
 libiberty/lrealpath.c
 libiberty/make-relative-prefix.c
 libiberty/md5.c
 libiberty/memmem.c
 libiberty/objalloc.c
 libiberty/partition.c
 libiberty/physmem.c
 libiberty/sha1.c
 libiberty/simple-object-coff.c
 libiberty/simple-object-elf.c
 libiberty/simple-object-mach-o.c
 libiberty/simple-object-xcoff.c
 libiberty/simple-object.c
 libiberty/snprintf.c
 libiberty/sort.c
 libiberty/splay-tree.c
 libiberty/stack-limit.c
 libiberty/strtod.c
 libiberty/unlink-if-ordinary.c
 libiberty/vsnprintf.c
 libiberty/vsprintf.c
Copyright: 1987-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-2+

Files: libiberty/acinclude.m4
Copyright: 1986-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3+

Files: libiberty/aclocal.m4
Copyright: 1996-2017, Free Software Foundation, Inc. <http://fsf.org/>
License: FSFULLR

Files: libiberty/basename.c
 libiberty/insque.c
 libiberty/strerror.c
 libiberty/strsignal.c
 libiberty/xstrerror.c
Copyright: no-info-found
License: public-domain

Files: libiberty/bsearch.c
 libiberty/bsearch_r.c
 libiberty/random.c
 libiberty/strtoul.c
 libiberty/strtoull.c
Copyright: 1983, 1990, 2014, Regents of the University of California.
License: BSD-3-clause

Files: libiberty/configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc. <http://fsf.org/>
License: FSFUL

Files: libiberty/cp-demangle.c
 libiberty/cp-demangle.h
 libiberty/cp-demint.c
 libiberty/crc32.c
 libiberty/dwarfnames.c
 libiberty/dyn-string.c
Copyright: 1998-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-2+ with GCC-exception-2.0 exception

Files: libiberty/cplus-dem.c
 libiberty/d-demangle.c
 libiberty/rust-demangle.c
Copyright: 1989-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: LGPL-2+ with GCC-exception-2.0 exception

Files: libiberty/libiberty.texi
Copyright: 2001-2024, Free Software Foundation, Inc.
License: GFDL-1.3+ or LGPL

Files: libiberty/obstack.c
 libiberty/regex.c
 libiberty/strverscmp.c
Copyright: 1985-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: LGPL-2.1+

Files: libiberty/strcasecmp.c
 libiberty/strncasecmp.c
Copyright: 1987, Regents of the University of California.
License: NTP

Files: libiberty/strtol.c
 libiberty/strtoll.c
Copyright: 1990, 2014, The Regents of the University of California.
License: BSD-3-clause

Files: libiberty/testsuite/Makefile.in
Copyright: 1989-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: LGPL-2+

Files: libiberty/testsuite/demangler-fuzzer.c
Copyright: 1986-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3+

Files: libiberty/testsuite/test-demangle.c
 libiberty/testsuite/test-pexecute.c
Copyright: 1987-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-2+

Files: libiberty/testsuite/test-expandargv.c
 libiberty/testsuite/test-strtol.c
Copyright: 1998-2024, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-2+ with GCC-exception-2.0 exception

Files: include/coff/sym.h include/coff/symconst.h include/opcode/ChangeLog-9103
Copyright: Copyright (C) 1990-2022 Free Software Foundation
License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

Files: libiberty/ChangeLog libiberty/vfprintf.c libiberty/xatexit.c
Copyright: Copyright (C) 1990-2023 Free Software Foundation
License: LGPL-2.1+
 Libiberty is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 Libiberty is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
