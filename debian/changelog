libiberty (20241020-1+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 08 Mar 2025 04:50:47 +0000

libiberty (20241020-1) unstable; urgency=medium

  * Update to 20241020.

 -- Matthias Klose <doko@debian.org>  Wed, 20 Nov 2024 14:32:23 +0100

libiberty (20240117-1) unstable; urgency=medium

  * Update to 20240117.
  * CVE-2021-3826 was already fixed in 20211102-1.

 -- Matthias Klose <doko@debian.org>  Wed, 17 Jan 2024 20:36:37 +0100

libiberty (20230721-1) unstable; urgency=medium

  * Update to 20230721.
  * Bump standards version.

 -- Matthias Klose <doko@debian.org>  Fri, 21 Jul 2023 14:31:20 +0200

libiberty (20230104-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 06 Apr 2023 11:45:19 +0000

libiberty (20230104-1) unstable; urgency=medium

  * Update to 20230104.
  * Add a DEP-8 test leveraging some functionality of libiberty (John Scott).
    Closes: #1014696.
  * Bump standards version.
  * Update copyright years.

 -- Matthias Klose <doko@debian.org>  Wed, 04 Jan 2023 08:56:31 +0100

libiberty (20220713-1) unstable; urgency=medium

  * Update to 20220713.

 -- Matthias Klose <doko@debian.org>  Wed, 13 Jul 2022 17:27:55 +0200

libiberty (20211102-1) unstable; urgency=medium

  * Update to 20210106.
  * Bump standards version.

 -- Matthias Klose <doko@debian.org>  Tue, 02 Nov 2021 15:09:37 +0200

libiberty (20210106-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 19:19:19 +0000

libiberty (20210106-1) unstable; urgency=medium

  * Update to 20210106.
  * Bump standards version.

 -- Matthias Klose <doko@debian.org>  Wed, 06 Jan 2021 11:30:09 +0100

libiberty (20201110-1) unstable; urgency=medium

  * Update to 20201110.
  * Bump debhelper version.

 -- Matthias Klose <doko@debian.org>  Tue, 10 Nov 2020 07:54:53 +0100

libiberty (20200409-1) unstable; urgency=medium

  * Update to 20200409.
  * Bump standards version.

 -- Matthias Klose <doko@debian.org>  Thu, 09 Apr 2020 12:53:12 +0200

libiberty (20190907-1) unstable; urgency=medium

  * Update to 20190907.
  * Bump standards version.
  * Build using dh-autoreconf.

 -- Matthias Klose <doko@debian.org>  Sat, 07 Sep 2019 09:14:45 +0200

libiberty (20190122-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 01:04:47 +0000

libiberty (20190122-1) unstable; urgency=medium

  * Update to 20190122.

 -- Matthias Klose <doko@debian.org>  Tue, 22 Jan 2019 11:15:42 +0100

libiberty (20181121-1) unstable; urgency=medium

  * Update to 20181121.

 -- Matthias Klose <doko@debian.org>  Wed, 21 Nov 2018 09:53:04 +0100

libiberty (20180614-1) unstable; urgency=medium

  * Update to 20180614.

 -- Matthias Klose <doko@debian.org>  Thu, 14 Jun 2018 00:49:36 +0200

libiberty (20180425-1) unstable; urgency=medium

  * Update to 20180425.

 -- Matthias Klose <doko@debian.org>  Wed, 25 Apr 2018 14:00:56 +0200

libiberty (20170913-1) unstable; urgency=medium

  * Update to 20170913.

 -- Matthias Klose <doko@debian.org>  Wed, 13 Sep 2017 12:42:06 +0200

libiberty (20170627-1) unstable; urgency=medium

  * Update to 20170627.
    - Addresses CVE-2016-4491.

 -- Matthias Klose <doko@debian.org>  Tue, 27 Jun 2017 15:52:55 +0200

libiberty (20161220-1) unstable; urgency=medium

  * Update to 20161220.

 -- Matthias Klose <doko@debian.org>  Tue, 20 Dec 2016 12:25:09 +0100

libiberty (20161017-1) unstable; urgency=medium

  * Update to 20161017 (CVE-2016-6131). Closes: #840889.
  * Don't apply "fixes" which are not yet accepted upstream.

 -- Matthias Klose <doko@debian.org>  Mon, 17 Oct 2016 11:37:08 +0200

libiberty (20161011-1) unstable; urgency=medium

  * Update to 20161011 (security issues fixed: CVE-2016-6131, CVE-2016-4493,
    CVE-2016-4492, CVE-2016-4491, CVE-2016-4490, CVE-2016-4489, CVE-2016-4488,
    CVE-2016-4487, CVE-2016-2226. Closes: #840360.

 -- Matthias Klose <doko@debian.org>  Tue, 11 Oct 2016 09:14:23 +0200

libiberty (20160807-1) unstable; urgency=medium

  * Update to 20160807.

 -- Matthias Klose <doko@debian.org>  Sun, 07 Aug 2016 14:03:33 +0200

libiberty (20160215-1) unstable; urgency=medium

  * Update to 20160215.

 -- Matthias Klose <doko@debian.org>  Mon, 15 Feb 2016 20:15:28 +0100

libiberty (20141014-1) unstable; urgency=medium

  * Update to 20141014.

 -- Matthias Klose <doko@debian.org>  Tue, 14 Oct 2014 14:23:53 +0200

libiberty (20140612-1) unstable; urgency=medium

  * Update to 20140612.
    - Fixes infinite recursion in the demangler. PR gdb/14963, LP: #1315590.
  * Use dh_autotools-dev to update config.{sub,guess}. Closes: #743673.

 -- Matthias Klose <doko@debian.org>  Thu, 12 Jun 2014 16:25:23 +0200

libiberty (20131116-1) unstable; urgency=low

  * Initial release.

 -- Matthias Klose <doko@debian.org>  Sat, 16 Nov 2013 19:30:20 +0000
